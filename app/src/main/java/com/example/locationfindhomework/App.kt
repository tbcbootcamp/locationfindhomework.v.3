package com.example.locationfindhomework

import android.app.Application

class App : Application() {


    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    private val INSTANCE: App by lazy {
        this
    }

    companion object {
        var instance: App? = null
    }

    fun getApiKey() = "AIzaSyBADWUmhO9XNVF_-qSZR6RQWcoHfSpAr6E"

}