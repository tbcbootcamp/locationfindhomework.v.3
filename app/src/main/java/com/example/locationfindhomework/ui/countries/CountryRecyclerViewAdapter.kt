package com.example.locationfindhomework.ui.countries

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.locationfindhomework.R
import kotlinx.android.synthetic.main.item_country_recyclerview_layout.view.*

class CountryRecyclerViewAdapter(private val countries: MutableList<CountryModel.Result>) :
    RecyclerView.Adapter<CountryRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_country_recyclerview_layout,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        return holder.onBind()
    }

    override fun getItemCount() = countries.size

    private lateinit var model: CountryModel.Result
    var selectedPosition = 0

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        fun onBind() {
            model = countries[adapterPosition]
            itemView.countryTextView.text = model.name
            itemView.setOnClickListener(this)
            if (adapterPosition == selectedPosition)
                itemView.selectImageView.visibility = View.VISIBLE
            else {
                itemView.selectImageView.visibility = View.INVISIBLE
            }
        }

        override fun onClick(v: View?) {
            selectedPosition = adapterPosition
            notifyDataSetChanged()

        }
    }
}