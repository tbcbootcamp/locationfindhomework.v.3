package com.example.locationfindhomework.ui.search

interface ItemClick {

    fun onClick(position: Int)

}