package com.example.locationfindhomework.ui.countries

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
data class CountryModel(var results: MutableList<Result>) : Parcelable {
    @Parcelize
    data class Result(
        var name: String,
        var iso2: String
    ) : Parcelable
}

