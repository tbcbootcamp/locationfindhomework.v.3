package com.example.locationfindhomework.ui.countries

import android.app.Activity
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.locationfindhomework.R
import kotlinx.android.synthetic.main.activity_choose_country.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class ChooseCountryActivity : AppCompatActivity() {
    private lateinit var adapter: CountryRecyclerViewAdapter
    private val countries = mutableListOf<CountryModel.Result>()
    private var countryId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_country)
        attachToolbar()
        init()
    }

    private fun attachToolbar() {
        setSupportActionBar(toolbarLayout)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbarTitle.text = getString(R.string.countries)
        doneButton.text = getString(R.string.finallize)
        doneButton.setOnClickListener {
            finalize()
        }
    }

    private fun init() {
        countries.addAll(intent?.extras!!.getParcelableArrayList("countries")!!)
        countryId = intent!!.extras!!.getString("countryId", "")
        intent!!.extras!!.clear()


        adapter = CountryRecyclerViewAdapter(countries)
        countryRecyclerView.layoutManager = LinearLayoutManager(this) as RecyclerView.LayoutManager?
        countryRecyclerView.adapter = adapter
        countryRecyclerView.setHasFixedSize(true)
        countryRecyclerView.scrollToPosition(adapter.selectedPosition)


        (0 until countries.size).forEach {
            if (countryId == countries[it].iso2) {
                adapter.selectedPosition = it
                return
            }
        }

    }


    private fun finalize() {
        val model = countries[adapter.selectedPosition]
        val intent = intent
        intent.putExtra("countryId", model.iso2)
        intent.putExtra("countryName", model.name)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

}
