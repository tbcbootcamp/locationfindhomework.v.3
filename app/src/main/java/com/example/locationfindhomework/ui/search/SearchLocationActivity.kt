package com.example.locationfindhomework.ui.search

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.locationfindhomework.App
import com.example.locationfindhomework.R
import com.example.locationfindhomework.dataloader.ApiHandler
import com.example.locationfindhomework.dataloader.CustomCallback
import com.example.locationfindhomework.ui.address.AddAddressActivity
import kotlinx.android.synthetic.main.activity_search_location.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.json.JSONObject

class SearchLocationActivity : AppCompatActivity() {
    private lateinit var searchRecyclerViewAdapter: LocationRecyclerViewAdapter
    private var addresses = mutableListOf<LocationModel>()
    private var countryId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_location)
        init()
    }

    private fun init() {
        setSupportActionBar(toolbarLayout)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbarTitle.text = getString(R.string.search_for_a_location)
        doneButton.text = getString(R.string.add)


        searchRecyclerViewAdapter = LocationRecyclerViewAdapter(
            addresses, itemClick
        )
        countryId = intent!!.extras!!.getString("countryId", "")
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = searchRecyclerViewAdapter
        searchEditText.addTextChangedListener(textWatcher)

        doneButton.setOnClickListener {
            val intent = Intent(this, AddAddressActivity::class.java)
            startActivity(intent)
        }
    }

    private fun getAddresses(input: String) {
        addresses.clear()
        searchRecyclerViewAdapter.notifyDataSetChanged()
        if (input.isEmpty())
            return
        val parameters = mutableMapOf<String, String>()
        parameters["input"] = input
        parameters["key"] = App.instance!!.getApiKey()
        parameters["language"] = "en"
        parameters["components"] = "country:$countryId"
        Log.d("countryId address", countryId)
        ApiHandler.getRequest(
            ApiHandler.AUTOCOMPLETE, parameters, object :
                CustomCallback {
                override fun onFailure(response: String) {
                    Toast.makeText(applicationContext, "failure", Toast.LENGTH_SHORT).show()
                }

                override fun onResponse(response: String) {
                    val json = JSONObject(response)
                    if (json.has("predictions")) {
                        val predictions = json.getJSONArray("predictions")
                        Log.d("on response", "yes")
                        (0 until predictions.length()).forEach {
                            val prediction = predictions.getJSONObject(it)
                            addresses.add(
                                LocationModel(
                                    prediction.getString("description"),
                                    prediction.getString("place_id")
                                )
                            )
                        }
                    }
                    searchRecyclerViewAdapter.notifyDataSetChanged()
                }

            })

    }

    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {

        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            getAddresses(s.toString())
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            super.onBackPressed()
        return true
    }

    private val itemClick = object : ItemClick {
        override fun onClick(position: Int) {
            val intent = intent
            intent.putExtra("streetAddress", addresses[position].description)
            intent.putExtra("placeId", addresses[position].placeId)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }
}