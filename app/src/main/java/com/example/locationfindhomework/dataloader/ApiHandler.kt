package com.example.locationfindhomework.dataloader

import android.util.Log
import com.example.locationfindhomework.App
import com.example.locationfindhomework.ui.search.AddressComps
import com.google.gson.Gson
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.QueryMap

object ApiHandler {
    const val AUTOCOMPLETE = "autocomplete"
    const val DETAILS = "details"

    private var retrofit = Retrofit.Builder()
        .baseUrl("https://maps.googleapis.com/maps/api/")
        .addConverterFactory(ScalarsConverterFactory.create())
        .build()

    private var service = retrofit.create(
        ApiService::class.java
    )

    interface ApiService {
        @GET("place/{path}/json")
        fun getRequest(
            @Path("path") path: String,
            @QueryMap parameters: MutableMap<String, String>
        ): Call<String>
    }

    fun getRequest(path: String, parameters: MutableMap<String, String>, callback: CustomCallback) {
        val call = service.getRequest(path, parameters)
        call.enqueue(
            onCallback(
                callback
            )
        )
    }


    private fun onCallback(callback: CustomCallback) = object : Callback<String> {
        override fun onFailure(call: Call<String>, t: Throwable) {
            callback.onFailure(t.message.toString())

        }

        override fun onResponse(call: Call<String>, response: Response<String>) {
            callback.onResponse(response.body().toString())

        }

    }

    fun getAddressDetail(
        placeID: String,
        callback: StreetsCallback<MutableMap<String, String>>
    ) {
        val parameter: HashMap<String, String> = HashMap()
        parameter["placeid"] = placeID
        parameter["key"] = App.instance!!.getApiKey()
        getRequest(DETAILS, parameter, object : CustomCallback {
            override fun onResponse(response: String) {
                val resultJson = JSONObject(response)
                Log.d("getAddressDetail ", response)
                if (resultJson.has("status")) {
                    if (resultJson.getString("status") == "OK")
                        if (resultJson.has("result"))
                            if (resultJson.getJSONObject("result").has("address_components")) {
                                val listingAddressComponents: List<AddressComps>? =
                                    Gson().fromJson(
                                        resultJson.getJSONObject("result")
                                            .getJSONArray("address_components").toString(),
                                        Array<AddressComps>::class.java
                                    ).toList()
                                var lat = 0.0
                                var lng = 0.0
                                if (resultJson.getJSONObject("result").has("geometry")) {
                                    val location =
                                        resultJson.getJSONObject("result").getJSONObject("geometry")
                                            .getJSONObject("location")
                                    lat = location.getDouble("lat")
                                    lng = location.getDouble("lng")
                                }
                                val resultsMap = HashMap<String, String>()
                                resultsMap["lat"] = lat.toString()
                                resultsMap["lng"] = lng.toString()
                                for (item in listingAddressComponents!!) {
                                    when {
                                        item.types.contains("street_number") -> resultsMap["streetNumber"] =
                                            item.longName
                                        item.types.contains("country") -> resultsMap["county"] =
                                            item.longName
                                        item.types.contains("postal_code") -> resultsMap["zipcode"] =
                                            item.longName
                                        item.types.contains("locality") -> resultsMap["city"] =
                                            item.longName
                                        item.types.contains("route") -> resultsMap["street"] =
                                            item.longName
                                        item.types.contains("administrative_area_level_1") -> resultsMap["state"] =
                                            item.longName
                                    }
                                }
                                callback.doneResults(resultsMap)
                            }
                }
            }

            override fun onFailure(response: String) {

            }
        })
    }


}