package com.example.locationfindhomework.dataloader

interface StreetsCallback<T> {
    fun doneResults(results: MutableMap<String, String>)
}